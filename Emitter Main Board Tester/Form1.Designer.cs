﻿namespace Emitter_Main_Board_Tester
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button_Disconect = new System.Windows.Forms.Button();
            this.button_Connect = new System.Windows.Forms.Button();
            this.label28 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.button_Rescan = new System.Windows.Forms.Button();
            this.checkBox00 = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonLED_RX = new System.Windows.Forms.Button();
            this.label43 = new System.Windows.Forms.Label();
            this.buttonLED_TX = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label50 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.textBox_IMU0 = new System.Windows.Forms.TextBox();
            this.textBox_IMU8 = new System.Windows.Forms.TextBox();
            this.textBox_IMU1 = new System.Windows.Forms.TextBox();
            this.textBox_IMU7 = new System.Windows.Forms.TextBox();
            this.textBox_IMU2 = new System.Windows.Forms.TextBox();
            this.textBox_IMU6 = new System.Windows.Forms.TextBox();
            this.textBox_IMU3 = new System.Windows.Forms.TextBox();
            this.textBox_IMU5 = new System.Windows.Forms.TextBox();
            this.textBox_IMU4 = new System.Windows.Forms.TextBox();
            this.textBox09_R = new System.Windows.Forms.TextBox();
            this.textBox03_1_R = new System.Windows.Forms.TextBox();
            this.textBox03_0_R = new System.Windows.Forms.TextBox();
            this.textBox04_2_R = new System.Windows.Forms.TextBox();
            this.textBox04_1_R = new System.Windows.Forms.TextBox();
            this.textBox04_0_R = new System.Windows.Forms.TextBox();
            this.textBox05_2_R = new System.Windows.Forms.TextBox();
            this.textBox05_1_R = new System.Windows.Forms.TextBox();
            this.textBox07_1_R = new System.Windows.Forms.TextBox();
            this.textBox00_R = new System.Windows.Forms.TextBox();
            this.textBox01_R = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.textBox02_R = new System.Windows.Forms.TextBox();
            this.textBox0C_R = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.textBox0B_R = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.textBox0A_R = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.textBox05_0_R = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.textBox06_R = new System.Windows.Forms.TextBox();
            this.textBox07_0_R = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.textBox08_R = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label47 = new System.Windows.Forms.Label();
            this.textBoxMD = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.textBoxREV = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.textBoxSN = new System.Windows.Forms.TextBox();
            this.textBox_calc_send = new System.Windows.Forms.TextBox();
            this.textBox09_1 = new System.Windows.Forms.TextBox();
            this.textBox07_1 = new System.Windows.Forms.TextBox();
            this.textBox00 = new System.Windows.Forms.TextBox();
            this.textBox01 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.textBox02 = new System.Windows.Forms.TextBox();
            this.textBox0C = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.textBox03 = new System.Windows.Forms.TextBox();
            this.textBox0B = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.textBox04 = new System.Windows.Forms.TextBox();
            this.textBox0A = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.textBox05 = new System.Windows.Forms.TextBox();
            this.textBox09_0 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.textBox06 = new System.Windows.Forms.TextBox();
            this.textBox07_0 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.textBox08 = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label44 = new System.Windows.Forms.Label();
            this.buttonMD = new System.Windows.Forms.Button();
            this.checkBoxMD = new System.Windows.Forms.CheckBox();
            this.label45 = new System.Windows.Forms.Label();
            this.buttonREV = new System.Windows.Forms.Button();
            this.checkBoxREV = new System.Windows.Forms.CheckBox();
            this.label46 = new System.Windows.Forms.Label();
            this.buttonSN = new System.Windows.Forms.Button();
            this.checkBoxSN = new System.Windows.Forms.CheckBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.button_READALL = new System.Windows.Forms.Button();
            this.button_WRITEALL = new System.Windows.Forms.Button();
            this.button00 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBox01 = new System.Windows.Forms.CheckBox();
            this.label22 = new System.Windows.Forms.Label();
            this.button01 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.button0C = new System.Windows.Forms.Button();
            this.checkBox02 = new System.Windows.Forms.CheckBox();
            this.checkBox0C = new System.Windows.Forms.CheckBox();
            this.button02 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.checkBox03 = new System.Windows.Forms.CheckBox();
            this.button03 = new System.Windows.Forms.Button();
            this.button0B = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.checkBox0B = new System.Windows.Forms.CheckBox();
            this.checkBox04 = new System.Windows.Forms.CheckBox();
            this.button04 = new System.Windows.Forms.Button();
            this.label26 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.checkBox05 = new System.Windows.Forms.CheckBox();
            this.button0A = new System.Windows.Forms.Button();
            this.button05 = new System.Windows.Forms.Button();
            this.checkBox0A = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.checkBox06 = new System.Windows.Forms.CheckBox();
            this.label18 = new System.Windows.Forms.Label();
            this.button06 = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.button09 = new System.Windows.Forms.Button();
            this.checkBox07 = new System.Windows.Forms.CheckBox();
            this.checkBox09 = new System.Windows.Forms.CheckBox();
            this.button07 = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.checkBox08 = new System.Windows.Forms.CheckBox();
            this.button08 = new System.Windows.Forms.Button();
            this.button_start_test = new System.Windows.Forms.Button();
            this.label27 = new System.Windows.Forms.Label();
            this.textBox_terminal = new System.Windows.Forms.TextBox();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.timer1_btn_rx = new System.Windows.Forms.Timer(this.components);
            this.timer_RX = new System.Windows.Forms.Timer(this.components);
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button_Disconect);
            this.groupBox1.Controls.Add(this.button_Connect);
            this.groupBox1.Controls.Add(this.label28);
            this.groupBox1.Controls.Add(this.comboBox1);
            this.groupBox1.Controls.Add(this.button_Rescan);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(376, 64);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "COM Port";
            // 
            // button_Disconect
            // 
            this.button_Disconect.Enabled = false;
            this.button_Disconect.Location = new System.Drawing.Point(297, 18);
            this.button_Disconect.Name = "button_Disconect";
            this.button_Disconect.Size = new System.Drawing.Size(75, 23);
            this.button_Disconect.TabIndex = 3;
            this.button_Disconect.Text = "Disconnect";
            this.button_Disconect.UseVisualStyleBackColor = true;
            this.button_Disconect.Click += new System.EventHandler(this.button_Disconect_Click);
            // 
            // button_Connect
            // 
            this.button_Connect.Enabled = false;
            this.button_Connect.Location = new System.Drawing.Point(216, 18);
            this.button_Connect.Name = "button_Connect";
            this.button_Connect.Size = new System.Drawing.Size(75, 23);
            this.button_Connect.TabIndex = 2;
            this.button_Connect.Text = "Connect";
            this.button_Connect.UseVisualStyleBackColor = true;
            this.button_Connect.Click += new System.EventHandler(this.button_Connect_Click);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(7, 45);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(256, 13);
            this.label28.TabIndex = 66;
            this.label28.Text = "Write in register in HEX mode  00-FF     115200b 8n1";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(88, 20);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 1;
            // 
            // button_Rescan
            // 
            this.button_Rescan.Location = new System.Drawing.Point(6, 19);
            this.button_Rescan.Name = "button_Rescan";
            this.button_Rescan.Size = new System.Drawing.Size(75, 23);
            this.button_Rescan.TabIndex = 0;
            this.button_Rescan.Text = "Rescan";
            this.button_Rescan.UseVisualStyleBackColor = true;
            this.button_Rescan.Click += new System.EventHandler(this.button_Rescan_Click);
            // 
            // checkBox00
            // 
            this.checkBox00.AutoSize = true;
            this.checkBox00.Location = new System.Drawing.Point(43, 25);
            this.checkBox00.Name = "checkBox00";
            this.checkBox00.Size = new System.Drawing.Size(15, 14);
            this.checkBox00.TabIndex = 1;
            this.checkBox00.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.buttonLED_RX);
            this.groupBox2.Controls.Add(this.label43);
            this.groupBox2.Controls.Add(this.buttonLED_TX);
            this.groupBox2.Controls.Add(this.groupBox5);
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.button_start_test);
            this.groupBox2.Location = new System.Drawing.Point(13, 83);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(544, 611);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Tested Registr";
            // 
            // buttonLED_RX
            // 
            this.buttonLED_RX.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.buttonLED_RX.Enabled = false;
            this.buttonLED_RX.Location = new System.Drawing.Point(303, 561);
            this.buttonLED_RX.Name = "buttonLED_RX";
            this.buttonLED_RX.Size = new System.Drawing.Size(57, 44);
            this.buttonLED_RX.TabIndex = 72;
            this.buttonLED_RX.Text = "RX";
            this.buttonLED_RX.UseVisualStyleBackColor = false;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(235, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(102, 13);
            this.label43.TabIndex = 68;
            this.label43.Text = "Use only 0-F in HEX";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonLED_TX
            // 
            this.buttonLED_TX.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.buttonLED_TX.Enabled = false;
            this.buttonLED_TX.Location = new System.Drawing.Point(233, 561);
            this.buttonLED_TX.Name = "buttonLED_TX";
            this.buttonLED_TX.Size = new System.Drawing.Size(57, 44);
            this.buttonLED_TX.TabIndex = 71;
            this.buttonLED_TX.Text = "TX";
            this.buttonLED_TX.UseVisualStyleBackColor = false;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label50);
            this.groupBox5.Controls.Add(this.textBox1);
            this.groupBox5.Controls.Add(this.label51);
            this.groupBox5.Controls.Add(this.textBox2);
            this.groupBox5.Controls.Add(this.label52);
            this.groupBox5.Controls.Add(this.textBox3);
            this.groupBox5.Controls.Add(this.groupBox6);
            this.groupBox5.Controls.Add(this.textBox09_R);
            this.groupBox5.Controls.Add(this.textBox03_1_R);
            this.groupBox5.Controls.Add(this.textBox03_0_R);
            this.groupBox5.Controls.Add(this.textBox04_2_R);
            this.groupBox5.Controls.Add(this.textBox04_1_R);
            this.groupBox5.Controls.Add(this.textBox04_0_R);
            this.groupBox5.Controls.Add(this.textBox05_2_R);
            this.groupBox5.Controls.Add(this.textBox05_1_R);
            this.groupBox5.Controls.Add(this.textBox07_1_R);
            this.groupBox5.Controls.Add(this.textBox00_R);
            this.groupBox5.Controls.Add(this.textBox01_R);
            this.groupBox5.Controls.Add(this.label29);
            this.groupBox5.Controls.Add(this.label30);
            this.groupBox5.Controls.Add(this.label31);
            this.groupBox5.Controls.Add(this.textBox02_R);
            this.groupBox5.Controls.Add(this.textBox0C_R);
            this.groupBox5.Controls.Add(this.label32);
            this.groupBox5.Controls.Add(this.label33);
            this.groupBox5.Controls.Add(this.textBox0B_R);
            this.groupBox5.Controls.Add(this.label34);
            this.groupBox5.Controls.Add(this.label35);
            this.groupBox5.Controls.Add(this.textBox0A_R);
            this.groupBox5.Controls.Add(this.label36);
            this.groupBox5.Controls.Add(this.textBox05_0_R);
            this.groupBox5.Controls.Add(this.label38);
            this.groupBox5.Controls.Add(this.label39);
            this.groupBox5.Controls.Add(this.textBox06_R);
            this.groupBox5.Controls.Add(this.textBox07_0_R);
            this.groupBox5.Controls.Add(this.label40);
            this.groupBox5.Controls.Add(this.label41);
            this.groupBox5.Controls.Add(this.textBox08_R);
            this.groupBox5.Location = new System.Drawing.Point(369, 21);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(166, 584);
            this.groupBox5.TabIndex = 70;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Read Data";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(85, 461);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(32, 13);
            this.label50.TabIndex = 88;
            this.label50.Text = "string";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox1.Location = new System.Drawing.Point(6, 458);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(73, 20);
            this.textBox1.TabIndex = 87;
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(85, 432);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(32, 13);
            this.label51.TabIndex = 86;
            this.label51.Text = "string";
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox2.Location = new System.Drawing.Point(6, 429);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(73, 20);
            this.textBox2.TabIndex = 85;
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(85, 403);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(32, 13);
            this.label52.TabIndex = 84;
            this.label52.Text = "string";
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox3.Location = new System.Drawing.Point(6, 401);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(73, 20);
            this.textBox3.TabIndex = 83;
            this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.textBox_IMU0);
            this.groupBox6.Controls.Add(this.textBox_IMU8);
            this.groupBox6.Controls.Add(this.textBox_IMU1);
            this.groupBox6.Controls.Add(this.textBox_IMU7);
            this.groupBox6.Controls.Add(this.textBox_IMU2);
            this.groupBox6.Controls.Add(this.textBox_IMU6);
            this.groupBox6.Controls.Add(this.textBox_IMU3);
            this.groupBox6.Controls.Add(this.textBox_IMU5);
            this.groupBox6.Controls.Add(this.textBox_IMU4);
            this.groupBox6.Location = new System.Drawing.Point(6, 481);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(126, 95);
            this.groupBox6.TabIndex = 71;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "IMU Data";
            // 
            // textBox_IMU0
            // 
            this.textBox_IMU0.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox_IMU0.Location = new System.Drawing.Point(6, 17);
            this.textBox_IMU0.Name = "textBox_IMU0";
            this.textBox_IMU0.ReadOnly = true;
            this.textBox_IMU0.Size = new System.Drawing.Size(33, 20);
            this.textBox_IMU0.TabIndex = 48;
            this.textBox_IMU0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_IMU8
            // 
            this.textBox_IMU8.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox_IMU8.Location = new System.Drawing.Point(85, 69);
            this.textBox_IMU8.Name = "textBox_IMU8";
            this.textBox_IMU8.ReadOnly = true;
            this.textBox_IMU8.Size = new System.Drawing.Size(33, 20);
            this.textBox_IMU8.TabIndex = 81;
            this.textBox_IMU8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_IMU1
            // 
            this.textBox_IMU1.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox_IMU1.Location = new System.Drawing.Point(46, 17);
            this.textBox_IMU1.Name = "textBox_IMU1";
            this.textBox_IMU1.ReadOnly = true;
            this.textBox_IMU1.Size = new System.Drawing.Size(33, 20);
            this.textBox_IMU1.TabIndex = 67;
            this.textBox_IMU1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_IMU7
            // 
            this.textBox_IMU7.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox_IMU7.Location = new System.Drawing.Point(46, 69);
            this.textBox_IMU7.Name = "textBox_IMU7";
            this.textBox_IMU7.ReadOnly = true;
            this.textBox_IMU7.Size = new System.Drawing.Size(33, 20);
            this.textBox_IMU7.TabIndex = 80;
            this.textBox_IMU7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_IMU2
            // 
            this.textBox_IMU2.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox_IMU2.Location = new System.Drawing.Point(85, 17);
            this.textBox_IMU2.Name = "textBox_IMU2";
            this.textBox_IMU2.ReadOnly = true;
            this.textBox_IMU2.Size = new System.Drawing.Size(33, 20);
            this.textBox_IMU2.TabIndex = 75;
            this.textBox_IMU2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_IMU6
            // 
            this.textBox_IMU6.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox_IMU6.Location = new System.Drawing.Point(6, 69);
            this.textBox_IMU6.Name = "textBox_IMU6";
            this.textBox_IMU6.ReadOnly = true;
            this.textBox_IMU6.Size = new System.Drawing.Size(33, 20);
            this.textBox_IMU6.TabIndex = 79;
            this.textBox_IMU6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_IMU3
            // 
            this.textBox_IMU3.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox_IMU3.Location = new System.Drawing.Point(6, 43);
            this.textBox_IMU3.Name = "textBox_IMU3";
            this.textBox_IMU3.ReadOnly = true;
            this.textBox_IMU3.Size = new System.Drawing.Size(33, 20);
            this.textBox_IMU3.TabIndex = 76;
            this.textBox_IMU3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_IMU5
            // 
            this.textBox_IMU5.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox_IMU5.Location = new System.Drawing.Point(85, 43);
            this.textBox_IMU5.Name = "textBox_IMU5";
            this.textBox_IMU5.ReadOnly = true;
            this.textBox_IMU5.Size = new System.Drawing.Size(33, 20);
            this.textBox_IMU5.TabIndex = 78;
            this.textBox_IMU5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_IMU4
            // 
            this.textBox_IMU4.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox_IMU4.Location = new System.Drawing.Point(46, 43);
            this.textBox_IMU4.Name = "textBox_IMU4";
            this.textBox_IMU4.ReadOnly = true;
            this.textBox_IMU4.Size = new System.Drawing.Size(33, 20);
            this.textBox_IMU4.TabIndex = 77;
            this.textBox_IMU4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox09_R
            // 
            this.textBox09_R.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox09_R.Location = new System.Drawing.Point(6, 278);
            this.textBox09_R.Name = "textBox09_R";
            this.textBox09_R.ReadOnly = true;
            this.textBox09_R.Size = new System.Drawing.Size(153, 20);
            this.textBox09_R.TabIndex = 82;
            this.textBox09_R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox03_1_R
            // 
            this.textBox03_1_R.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox03_1_R.Location = new System.Drawing.Point(45, 112);
            this.textBox03_1_R.Name = "textBox03_1_R";
            this.textBox03_1_R.ReadOnly = true;
            this.textBox03_1_R.Size = new System.Drawing.Size(33, 20);
            this.textBox03_1_R.TabIndex = 74;
            this.textBox03_1_R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox03_0_R
            // 
            this.textBox03_0_R.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox03_0_R.Location = new System.Drawing.Point(6, 112);
            this.textBox03_0_R.Name = "textBox03_0_R";
            this.textBox03_0_R.ReadOnly = true;
            this.textBox03_0_R.Size = new System.Drawing.Size(33, 20);
            this.textBox03_0_R.TabIndex = 73;
            this.textBox03_0_R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox04_2_R
            // 
            this.textBox04_2_R.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox04_2_R.Location = new System.Drawing.Point(84, 137);
            this.textBox04_2_R.Name = "textBox04_2_R";
            this.textBox04_2_R.ReadOnly = true;
            this.textBox04_2_R.Size = new System.Drawing.Size(33, 20);
            this.textBox04_2_R.TabIndex = 72;
            this.textBox04_2_R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox04_1_R
            // 
            this.textBox04_1_R.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox04_1_R.Location = new System.Drawing.Point(45, 138);
            this.textBox04_1_R.Name = "textBox04_1_R";
            this.textBox04_1_R.ReadOnly = true;
            this.textBox04_1_R.Size = new System.Drawing.Size(33, 20);
            this.textBox04_1_R.TabIndex = 71;
            this.textBox04_1_R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox04_0_R
            // 
            this.textBox04_0_R.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox04_0_R.Location = new System.Drawing.Point(6, 138);
            this.textBox04_0_R.Name = "textBox04_0_R";
            this.textBox04_0_R.ReadOnly = true;
            this.textBox04_0_R.Size = new System.Drawing.Size(33, 20);
            this.textBox04_0_R.TabIndex = 70;
            this.textBox04_0_R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox05_2_R
            // 
            this.textBox05_2_R.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox05_2_R.Location = new System.Drawing.Point(84, 166);
            this.textBox05_2_R.Name = "textBox05_2_R";
            this.textBox05_2_R.ReadOnly = true;
            this.textBox05_2_R.Size = new System.Drawing.Size(33, 20);
            this.textBox05_2_R.TabIndex = 69;
            this.textBox05_2_R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox05_1_R
            // 
            this.textBox05_1_R.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox05_1_R.Location = new System.Drawing.Point(45, 167);
            this.textBox05_1_R.Name = "textBox05_1_R";
            this.textBox05_1_R.ReadOnly = true;
            this.textBox05_1_R.Size = new System.Drawing.Size(33, 20);
            this.textBox05_1_R.TabIndex = 68;
            this.textBox05_1_R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox07_1_R
            // 
            this.textBox07_1_R.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox07_1_R.Location = new System.Drawing.Point(46, 225);
            this.textBox07_1_R.Name = "textBox07_1_R";
            this.textBox07_1_R.ReadOnly = true;
            this.textBox07_1_R.Size = new System.Drawing.Size(33, 20);
            this.textBox07_1_R.TabIndex = 66;
            this.textBox07_1_R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox00_R
            // 
            this.textBox00_R.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox00_R.Location = new System.Drawing.Point(6, 22);
            this.textBox00_R.Name = "textBox00_R";
            this.textBox00_R.ReadOnly = true;
            this.textBox00_R.Size = new System.Drawing.Size(73, 20);
            this.textBox00_R.TabIndex = 3;
            this.textBox00_R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox01_R
            // 
            this.textBox01_R.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox01_R.Location = new System.Drawing.Point(6, 51);
            this.textBox01_R.Name = "textBox01_R";
            this.textBox01_R.ReadOnly = true;
            this.textBox01_R.Size = new System.Drawing.Size(73, 20);
            this.textBox01_R.TabIndex = 7;
            this.textBox01_R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(85, 24);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(36, 13);
            this.label29.TabIndex = 9;
            this.label29.Text = "1 byte";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(85, 53);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(36, 13);
            this.label30.TabIndex = 10;
            this.label30.Text = "1 byte";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(85, 372);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(36, 13);
            this.label31.TabIndex = 65;
            this.label31.Text = "1 byte";
            // 
            // textBox02_R
            // 
            this.textBox02_R.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox02_R.Location = new System.Drawing.Point(6, 80);
            this.textBox02_R.Name = "textBox02_R";
            this.textBox02_R.ReadOnly = true;
            this.textBox02_R.Size = new System.Drawing.Size(73, 20);
            this.textBox02_R.TabIndex = 13;
            this.textBox02_R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox0C_R
            // 
            this.textBox0C_R.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox0C_R.Location = new System.Drawing.Point(6, 366);
            this.textBox0C_R.Name = "textBox0C_R";
            this.textBox0C_R.ReadOnly = true;
            this.textBox0C_R.Size = new System.Drawing.Size(73, 20);
            this.textBox0C_R.TabIndex = 63;
            this.textBox0C_R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(85, 82);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(36, 13);
            this.label32.TabIndex = 15;
            this.label32.Text = "1 byte";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(85, 343);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(36, 13);
            this.label33.TabIndex = 60;
            this.label33.Text = "1 byte";
            // 
            // textBox0B_R
            // 
            this.textBox0B_R.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox0B_R.Location = new System.Drawing.Point(6, 337);
            this.textBox0B_R.Name = "textBox0B_R";
            this.textBox0B_R.ReadOnly = true;
            this.textBox0B_R.Size = new System.Drawing.Size(73, 20);
            this.textBox0B_R.TabIndex = 58;
            this.textBox0B_R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(85, 111);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(36, 13);
            this.label34.TabIndex = 20;
            this.label34.Text = "2 byte";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(85, 314);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(36, 13);
            this.label35.TabIndex = 55;
            this.label35.Text = "1 byte";
            // 
            // textBox0A_R
            // 
            this.textBox0A_R.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox0A_R.Location = new System.Drawing.Point(6, 309);
            this.textBox0A_R.Name = "textBox0A_R";
            this.textBox0A_R.ReadOnly = true;
            this.textBox0A_R.Size = new System.Drawing.Size(73, 20);
            this.textBox0A_R.TabIndex = 53;
            this.textBox0A_R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(123, 142);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(36, 13);
            this.label36.TabIndex = 25;
            this.label36.Text = "3 byte";
            // 
            // textBox05_0_R
            // 
            this.textBox05_0_R.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox05_0_R.Location = new System.Drawing.Point(6, 167);
            this.textBox05_0_R.Name = "textBox05_0_R";
            this.textBox05_0_R.ReadOnly = true;
            this.textBox05_0_R.Size = new System.Drawing.Size(33, 20);
            this.textBox05_0_R.TabIndex = 28;
            this.textBox05_0_R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(123, 171);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(36, 13);
            this.label38.TabIndex = 30;
            this.label38.Text = "6 byte";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(85, 256);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(36, 13);
            this.label39.TabIndex = 45;
            this.label39.Text = "1 byte";
            // 
            // textBox06_R
            // 
            this.textBox06_R.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox06_R.Location = new System.Drawing.Point(6, 196);
            this.textBox06_R.Name = "textBox06_R";
            this.textBox06_R.ReadOnly = true;
            this.textBox06_R.Size = new System.Drawing.Size(73, 20);
            this.textBox06_R.TabIndex = 33;
            this.textBox06_R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox07_0_R
            // 
            this.textBox07_0_R.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox07_0_R.Location = new System.Drawing.Point(6, 225);
            this.textBox07_0_R.Name = "textBox07_0_R";
            this.textBox07_0_R.ReadOnly = true;
            this.textBox07_0_R.Size = new System.Drawing.Size(33, 20);
            this.textBox07_0_R.TabIndex = 43;
            this.textBox07_0_R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(85, 198);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(36, 13);
            this.label40.TabIndex = 35;
            this.label40.Text = "1 byte";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(85, 227);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(36, 13);
            this.label41.TabIndex = 40;
            this.label41.Text = "2 byte";
            // 
            // textBox08_R
            // 
            this.textBox08_R.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox08_R.Location = new System.Drawing.Point(5, 250);
            this.textBox08_R.Name = "textBox08_R";
            this.textBox08_R.ReadOnly = true;
            this.textBox08_R.Size = new System.Drawing.Size(73, 20);
            this.textBox08_R.TabIndex = 38;
            this.textBox08_R.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label47);
            this.groupBox4.Controls.Add(this.textBoxMD);
            this.groupBox4.Controls.Add(this.label48);
            this.groupBox4.Controls.Add(this.textBoxREV);
            this.groupBox4.Controls.Add(this.label49);
            this.groupBox4.Controls.Add(this.textBoxSN);
            this.groupBox4.Controls.Add(this.textBox_calc_send);
            this.groupBox4.Controls.Add(this.textBox09_1);
            this.groupBox4.Controls.Add(this.textBox07_1);
            this.groupBox4.Controls.Add(this.textBox00);
            this.groupBox4.Controls.Add(this.textBox01);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this.textBox02);
            this.groupBox4.Controls.Add(this.textBox0C);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.label23);
            this.groupBox4.Controls.Add(this.textBox03);
            this.groupBox4.Controls.Add(this.textBox0B);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.label25);
            this.groupBox4.Controls.Add(this.textBox04);
            this.groupBox4.Controls.Add(this.textBox0A);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this.textBox05);
            this.groupBox4.Controls.Add(this.textBox09_0);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this.textBox06);
            this.groupBox4.Controls.Add(this.textBox07_0);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.textBox08);
            this.groupBox4.Location = new System.Drawing.Point(232, 21);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(131, 533);
            this.groupBox4.TabIndex = 69;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Write Data";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(86, 462);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(32, 13);
            this.label47.TabIndex = 89;
            this.label47.Text = "string";
            // 
            // textBoxMD
            // 
            this.textBoxMD.Location = new System.Drawing.Point(6, 459);
            this.textBoxMD.Name = "textBoxMD";
            this.textBoxMD.Size = new System.Drawing.Size(73, 20);
            this.textBoxMD.TabIndex = 88;
            this.textBoxMD.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(86, 433);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(32, 13);
            this.label48.TabIndex = 87;
            this.label48.Text = "string";
            // 
            // textBoxREV
            // 
            this.textBoxREV.Location = new System.Drawing.Point(6, 430);
            this.textBoxREV.Name = "textBoxREV";
            this.textBoxREV.Size = new System.Drawing.Size(73, 20);
            this.textBoxREV.TabIndex = 86;
            this.textBoxREV.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(86, 404);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(32, 13);
            this.label49.TabIndex = 85;
            this.label49.Text = "string";
            // 
            // textBoxSN
            // 
            this.textBoxSN.Location = new System.Drawing.Point(6, 402);
            this.textBoxSN.Name = "textBoxSN";
            this.textBoxSN.Size = new System.Drawing.Size(73, 20);
            this.textBoxSN.TabIndex = 84;
            this.textBoxSN.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox_calc_send
            // 
            this.textBox_calc_send.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.textBox_calc_send.Location = new System.Drawing.Point(6, 486);
            this.textBox_calc_send.Multiline = true;
            this.textBox_calc_send.Name = "textBox_calc_send";
            this.textBox_calc_send.Size = new System.Drawing.Size(119, 40);
            this.textBox_calc_send.TabIndex = 83;
            // 
            // textBox09_1
            // 
            this.textBox09_1.Location = new System.Drawing.Point(46, 280);
            this.textBox09_1.MaxLength = 2;
            this.textBox09_1.Name = "textBox09_1";
            this.textBox09_1.Size = new System.Drawing.Size(33, 20);
            this.textBox09_1.TabIndex = 67;
            this.textBox09_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox09_1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox01_KeyPress);
            // 
            // textBox07_1
            // 
            this.textBox07_1.Location = new System.Drawing.Point(46, 221);
            this.textBox07_1.MaxLength = 2;
            this.textBox07_1.Name = "textBox07_1";
            this.textBox07_1.Size = new System.Drawing.Size(33, 20);
            this.textBox07_1.TabIndex = 66;
            this.textBox07_1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox07_1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox01_KeyPress);
            // 
            // textBox00
            // 
            this.textBox00.Location = new System.Drawing.Point(6, 22);
            this.textBox00.Name = "textBox00";
            this.textBox00.ReadOnly = true;
            this.textBox00.Size = new System.Drawing.Size(73, 20);
            this.textBox00.TabIndex = 3;
            this.textBox00.Text = "READ ONLY";
            this.textBox00.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox01
            // 
            this.textBox01.Location = new System.Drawing.Point(6, 51);
            this.textBox01.MaxLength = 2;
            this.textBox01.Name = "textBox01";
            this.textBox01.Size = new System.Drawing.Size(73, 20);
            this.textBox01.TabIndex = 7;
            this.textBox01.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox01.TextChanged += new System.EventHandler(this.textBox01_TextChanged);
            this.textBox01.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox01_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(85, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "1 byte";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(85, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "1 byte";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(85, 372);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(36, 13);
            this.label21.TabIndex = 65;
            this.label21.Text = "1 byte";
            // 
            // textBox02
            // 
            this.textBox02.Location = new System.Drawing.Point(6, 80);
            this.textBox02.Name = "textBox02";
            this.textBox02.ReadOnly = true;
            this.textBox02.Size = new System.Drawing.Size(73, 20);
            this.textBox02.TabIndex = 13;
            this.textBox02.Text = "READ ONLY";
            this.textBox02.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox0C
            // 
            this.textBox0C.Location = new System.Drawing.Point(6, 366);
            this.textBox0C.MaxLength = 2;
            this.textBox0C.Name = "textBox0C";
            this.textBox0C.Size = new System.Drawing.Size(73, 20);
            this.textBox0C.TabIndex = 63;
            this.textBox0C.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox0C.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox01_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(85, 82);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "1 byte";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(85, 343);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(36, 13);
            this.label23.TabIndex = 60;
            this.label23.Text = "1 byte";
            // 
            // textBox03
            // 
            this.textBox03.Location = new System.Drawing.Point(6, 109);
            this.textBox03.Name = "textBox03";
            this.textBox03.ReadOnly = true;
            this.textBox03.Size = new System.Drawing.Size(73, 20);
            this.textBox03.TabIndex = 18;
            this.textBox03.Text = "READ ONLY";
            this.textBox03.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox0B
            // 
            this.textBox0B.Location = new System.Drawing.Point(6, 337);
            this.textBox0B.MaxLength = 2;
            this.textBox0B.Name = "textBox0B";
            this.textBox0B.Size = new System.Drawing.Size(73, 20);
            this.textBox0B.TabIndex = 58;
            this.textBox0B.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox0B.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox01_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(85, 111);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "2 byte";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(85, 314);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(36, 13);
            this.label25.TabIndex = 55;
            this.label25.Text = "1 byte";
            // 
            // textBox04
            // 
            this.textBox04.Location = new System.Drawing.Point(6, 138);
            this.textBox04.Name = "textBox04";
            this.textBox04.ReadOnly = true;
            this.textBox04.Size = new System.Drawing.Size(73, 20);
            this.textBox04.TabIndex = 23;
            this.textBox04.Text = "READ ONLY";
            this.textBox04.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox0A
            // 
            this.textBox0A.Location = new System.Drawing.Point(6, 309);
            this.textBox0A.MaxLength = 2;
            this.textBox0A.Name = "textBox0A";
            this.textBox0A.Size = new System.Drawing.Size(73, 20);
            this.textBox0A.TabIndex = 53;
            this.textBox0A.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox0A.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox01_KeyPress);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(85, 140);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(36, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "3 byte";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(85, 285);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(43, 13);
            this.label17.TabIndex = 50;
            this.label17.Text = "1-2Byte";
            // 
            // textBox05
            // 
            this.textBox05.Location = new System.Drawing.Point(6, 167);
            this.textBox05.Name = "textBox05";
            this.textBox05.ReadOnly = true;
            this.textBox05.Size = new System.Drawing.Size(73, 20);
            this.textBox05.TabIndex = 28;
            this.textBox05.Text = "READ ONLY";
            this.textBox05.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox09_0
            // 
            this.textBox09_0.Location = new System.Drawing.Point(6, 280);
            this.textBox09_0.MaxLength = 2;
            this.textBox09_0.Name = "textBox09_0";
            this.textBox09_0.Size = new System.Drawing.Size(33, 20);
            this.textBox09_0.TabIndex = 48;
            this.textBox09_0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox09_0.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox01_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(85, 169);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(36, 13);
            this.label9.TabIndex = 30;
            this.label9.Text = "6 byte";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(85, 256);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(36, 13);
            this.label19.TabIndex = 45;
            this.label19.Text = "1 byte";
            // 
            // textBox06
            // 
            this.textBox06.Location = new System.Drawing.Point(6, 196);
            this.textBox06.MaxLength = 2;
            this.textBox06.Name = "textBox06";
            this.textBox06.Size = new System.Drawing.Size(73, 20);
            this.textBox06.TabIndex = 33;
            this.textBox06.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox06.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox01_KeyPress);
            // 
            // textBox07_0
            // 
            this.textBox07_0.Location = new System.Drawing.Point(6, 221);
            this.textBox07_0.MaxLength = 2;
            this.textBox07_0.Name = "textBox07_0";
            this.textBox07_0.Size = new System.Drawing.Size(33, 20);
            this.textBox07_0.TabIndex = 43;
            this.textBox07_0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox07_0.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox01_KeyPress);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(85, 198);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(36, 13);
            this.label13.TabIndex = 35;
            this.label13.Text = "1 byte";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(85, 227);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(36, 13);
            this.label15.TabIndex = 40;
            this.label15.Text = "2 byte";
            // 
            // textBox08
            // 
            this.textBox08.Location = new System.Drawing.Point(6, 251);
            this.textBox08.MaxLength = 2;
            this.textBox08.Name = "textBox08";
            this.textBox08.Size = new System.Drawing.Size(73, 20);
            this.textBox08.TabIndex = 38;
            this.textBox08.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox08.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox01_KeyPress);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label44);
            this.groupBox3.Controls.Add(this.buttonMD);
            this.groupBox3.Controls.Add(this.checkBoxMD);
            this.groupBox3.Controls.Add(this.label45);
            this.groupBox3.Controls.Add(this.buttonREV);
            this.groupBox3.Controls.Add(this.checkBoxREV);
            this.groupBox3.Controls.Add(this.label46);
            this.groupBox3.Controls.Add(this.buttonSN);
            this.groupBox3.Controls.Add(this.checkBoxSN);
            this.groupBox3.Controls.Add(this.label42);
            this.groupBox3.Controls.Add(this.label37);
            this.groupBox3.Controls.Add(this.button_READALL);
            this.groupBox3.Controls.Add(this.button_WRITEALL);
            this.groupBox3.Controls.Add(this.button00);
            this.groupBox3.Controls.Add(this.checkBox00);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.checkBox01);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Controls.Add(this.button01);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.button0C);
            this.groupBox3.Controls.Add(this.checkBox02);
            this.groupBox3.Controls.Add(this.checkBox0C);
            this.groupBox3.Controls.Add(this.button02);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label24);
            this.groupBox3.Controls.Add(this.checkBox03);
            this.groupBox3.Controls.Add(this.button03);
            this.groupBox3.Controls.Add(this.button0B);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.checkBox0B);
            this.groupBox3.Controls.Add(this.checkBox04);
            this.groupBox3.Controls.Add(this.button04);
            this.groupBox3.Controls.Add(this.label26);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.checkBox05);
            this.groupBox3.Controls.Add(this.button0A);
            this.groupBox3.Controls.Add(this.button05);
            this.groupBox3.Controls.Add(this.checkBox0A);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.checkBox06);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.button06);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.button09);
            this.groupBox3.Controls.Add(this.checkBox07);
            this.groupBox3.Controls.Add(this.checkBox09);
            this.groupBox3.Controls.Add(this.button07);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.checkBox08);
            this.groupBox3.Controls.Add(this.button08);
            this.groupBox3.Location = new System.Drawing.Point(6, 21);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(220, 533);
            this.groupBox3.TabIndex = 68;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Controll Buttons";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(7, 463);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(30, 13);
            this.label44.TabIndex = 77;
            this.label44.Text = "0xF0";
            // 
            // buttonMD
            // 
            this.buttonMD.Location = new System.Drawing.Point(64, 457);
            this.buttonMD.Name = "buttonMD";
            this.buttonMD.Size = new System.Drawing.Size(148, 23);
            this.buttonMD.TabIndex = 76;
            this.buttonMD.Text = "Manufacturing Date";
            this.buttonMD.UseVisualStyleBackColor = true;
            // 
            // checkBoxMD
            // 
            this.checkBoxMD.AutoSize = true;
            this.checkBoxMD.Location = new System.Drawing.Point(43, 463);
            this.checkBoxMD.Name = "checkBoxMD";
            this.checkBoxMD.Size = new System.Drawing.Size(15, 14);
            this.checkBoxMD.TabIndex = 75;
            this.checkBoxMD.UseVisualStyleBackColor = true;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(7, 434);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(30, 13);
            this.label45.TabIndex = 74;
            this.label45.Text = "0xF0";
            // 
            // buttonREV
            // 
            this.buttonREV.Location = new System.Drawing.Point(64, 428);
            this.buttonREV.Name = "buttonREV";
            this.buttonREV.Size = new System.Drawing.Size(148, 23);
            this.buttonREV.TabIndex = 73;
            this.buttonREV.Text = "Revision";
            this.buttonREV.UseVisualStyleBackColor = true;
            // 
            // checkBoxREV
            // 
            this.checkBoxREV.AutoSize = true;
            this.checkBoxREV.Location = new System.Drawing.Point(43, 434);
            this.checkBoxREV.Name = "checkBoxREV";
            this.checkBoxREV.Size = new System.Drawing.Size(15, 14);
            this.checkBoxREV.TabIndex = 72;
            this.checkBoxREV.UseVisualStyleBackColor = true;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(7, 405);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(30, 13);
            this.label46.TabIndex = 71;
            this.label46.Text = "0xF0";
            // 
            // buttonSN
            // 
            this.buttonSN.Location = new System.Drawing.Point(64, 399);
            this.buttonSN.Name = "buttonSN";
            this.buttonSN.Size = new System.Drawing.Size(148, 23);
            this.buttonSN.TabIndex = 70;
            this.buttonSN.Text = "Serial Number";
            this.buttonSN.UseVisualStyleBackColor = true;
            // 
            // checkBoxSN
            // 
            this.checkBoxSN.AutoSize = true;
            this.checkBoxSN.Location = new System.Drawing.Point(43, 405);
            this.checkBoxSN.Name = "checkBoxSN";
            this.checkBoxSN.Size = new System.Drawing.Size(15, 14);
            this.checkBoxSN.TabIndex = 69;
            this.checkBoxSN.UseVisualStyleBackColor = true;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(115, 488);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(99, 13);
            this.label42.TabIndex = 68;
            this.label42.Text = "RIGHT click READ";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(3, 488);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(97, 13);
            this.label37.TabIndex = 67;
            this.label37.Text = "LEFT click WRITE";
            // 
            // button_READALL
            // 
            this.button_READALL.Location = new System.Drawing.Point(111, 504);
            this.button_READALL.Name = "button_READALL";
            this.button_READALL.Size = new System.Drawing.Size(99, 23);
            this.button_READALL.TabIndex = 66;
            this.button_READALL.Text = "READ ALL";
            this.button_READALL.UseVisualStyleBackColor = true;
            this.button_READALL.Click += new System.EventHandler(this.button_READALL_Click);
            // 
            // button_WRITEALL
            // 
            this.button_WRITEALL.Location = new System.Drawing.Point(6, 504);
            this.button_WRITEALL.Name = "button_WRITEALL";
            this.button_WRITEALL.Size = new System.Drawing.Size(99, 23);
            this.button_WRITEALL.TabIndex = 65;
            this.button_WRITEALL.Text = "WRITE ALL";
            this.button_WRITEALL.UseVisualStyleBackColor = true;
            this.button_WRITEALL.Click += new System.EventHandler(this.button_WRITEALL_Click);
            // 
            // button00
            // 
            this.button00.Location = new System.Drawing.Point(64, 19);
            this.button00.Name = "button00";
            this.button00.Size = new System.Drawing.Size(148, 23);
            this.button00.TabIndex = 2;
            this.button00.Text = "Device type";
            this.button00.UseVisualStyleBackColor = true;
            this.button00.Click += new System.EventHandler(this.button00_Click);
            this.button00.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button00_MouseDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.Control;
            this.label1.Location = new System.Drawing.Point(7, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "0x00";
            // 
            // checkBox01
            // 
            this.checkBox01.AutoSize = true;
            this.checkBox01.Location = new System.Drawing.Point(43, 54);
            this.checkBox01.Name = "checkBox01";
            this.checkBox01.Size = new System.Drawing.Size(15, 14);
            this.checkBox01.TabIndex = 5;
            this.checkBox01.UseVisualStyleBackColor = true;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(7, 373);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(31, 13);
            this.label22.TabIndex = 64;
            this.label22.Text = "0x0C";
            // 
            // button01
            // 
            this.button01.Location = new System.Drawing.Point(64, 48);
            this.button01.Name = "button01";
            this.button01.Size = new System.Drawing.Size(148, 23);
            this.button01.TabIndex = 6;
            this.button01.Text = "Event Mask Register";
            this.button01.UseVisualStyleBackColor = true;
            this.button01.Click += new System.EventHandler(this.button01_Click);
            this.button01.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button01_MouseDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "0x01";
            // 
            // button0C
            // 
            this.button0C.Location = new System.Drawing.Point(64, 367);
            this.button0C.Name = "button0C";
            this.button0C.Size = new System.Drawing.Size(148, 23);
            this.button0C.TabIndex = 62;
            this.button0C.Text = "HeartBeat In Frequency";
            this.button0C.UseVisualStyleBackColor = true;
            this.button0C.Click += new System.EventHandler(this.button0C_Click);
            this.button0C.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button0C_MouseDown);
            // 
            // checkBox02
            // 
            this.checkBox02.AutoSize = true;
            this.checkBox02.Location = new System.Drawing.Point(43, 83);
            this.checkBox02.Name = "checkBox02";
            this.checkBox02.Size = new System.Drawing.Size(15, 14);
            this.checkBox02.TabIndex = 11;
            this.checkBox02.UseVisualStyleBackColor = true;
            // 
            // checkBox0C
            // 
            this.checkBox0C.AutoSize = true;
            this.checkBox0C.Location = new System.Drawing.Point(43, 373);
            this.checkBox0C.Name = "checkBox0C";
            this.checkBox0C.Size = new System.Drawing.Size(15, 14);
            this.checkBox0C.TabIndex = 61;
            this.checkBox0C.UseVisualStyleBackColor = true;
            // 
            // button02
            // 
            this.button02.Location = new System.Drawing.Point(64, 77);
            this.button02.Name = "button02";
            this.button02.Size = new System.Drawing.Size(148, 23);
            this.button02.TabIndex = 12;
            this.button02.Text = "Event Flag Register";
            this.button02.UseVisualStyleBackColor = true;
            this.button02.Click += new System.EventHandler(this.button02_Click);
            this.button02.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button02_MouseDown);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 83);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "0x02";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(7, 344);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(31, 13);
            this.label24.TabIndex = 59;
            this.label24.Text = "0x0B";
            // 
            // checkBox03
            // 
            this.checkBox03.AutoSize = true;
            this.checkBox03.Location = new System.Drawing.Point(43, 112);
            this.checkBox03.Name = "checkBox03";
            this.checkBox03.Size = new System.Drawing.Size(15, 14);
            this.checkBox03.TabIndex = 16;
            this.checkBox03.UseVisualStyleBackColor = true;
            // 
            // button03
            // 
            this.button03.Location = new System.Drawing.Point(64, 106);
            this.button03.Name = "button03";
            this.button03.Size = new System.Drawing.Size(148, 23);
            this.button03.TabIndex = 17;
            this.button03.Text = "FW Version";
            this.button03.UseVisualStyleBackColor = true;
            this.button03.Click += new System.EventHandler(this.button03_Click);
            this.button03.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button03_MouseDown);
            // 
            // button0B
            // 
            this.button0B.Location = new System.Drawing.Point(64, 338);
            this.button0B.Name = "button0B";
            this.button0B.Size = new System.Drawing.Size(148, 23);
            this.button0B.TabIndex = 57;
            this.button0B.Text = "HeartBeat Out Frequency";
            this.button0B.UseVisualStyleBackColor = true;
            this.button0B.Click += new System.EventHandler(this.button0B_Click);
            this.button0B.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button0B_MouseDown);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 112);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(30, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "0x03";
            // 
            // checkBox0B
            // 
            this.checkBox0B.AutoSize = true;
            this.checkBox0B.Location = new System.Drawing.Point(43, 344);
            this.checkBox0B.Name = "checkBox0B";
            this.checkBox0B.Size = new System.Drawing.Size(15, 14);
            this.checkBox0B.TabIndex = 56;
            this.checkBox0B.UseVisualStyleBackColor = true;
            // 
            // checkBox04
            // 
            this.checkBox04.AutoSize = true;
            this.checkBox04.Location = new System.Drawing.Point(43, 141);
            this.checkBox04.Name = "checkBox04";
            this.checkBox04.Size = new System.Drawing.Size(15, 14);
            this.checkBox04.TabIndex = 21;
            this.checkBox04.UseVisualStyleBackColor = true;
            // 
            // button04
            // 
            this.button04.Location = new System.Drawing.Point(64, 135);
            this.button04.Name = "button04";
            this.button04.Size = new System.Drawing.Size(148, 23);
            this.button04.TabIndex = 22;
            this.button04.Text = "Thermistor Value";
            this.button04.UseVisualStyleBackColor = true;
            this.button04.Click += new System.EventHandler(this.button04_Click);
            this.button04.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button04_MouseDown);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(7, 315);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(31, 13);
            this.label26.TabIndex = 54;
            this.label26.Text = "0x0A";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 141);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(30, 13);
            this.label12.TabIndex = 24;
            this.label12.Text = "0x04";
            // 
            // checkBox05
            // 
            this.checkBox05.AutoSize = true;
            this.checkBox05.Location = new System.Drawing.Point(43, 170);
            this.checkBox05.Name = "checkBox05";
            this.checkBox05.Size = new System.Drawing.Size(15, 14);
            this.checkBox05.TabIndex = 26;
            this.checkBox05.UseVisualStyleBackColor = true;
            // 
            // button0A
            // 
            this.button0A.Location = new System.Drawing.Point(64, 309);
            this.button0A.Name = "button0A";
            this.button0A.Size = new System.Drawing.Size(148, 23);
            this.button0A.TabIndex = 52;
            this.button0A.Text = "Buzzer";
            this.button0A.UseVisualStyleBackColor = true;
            this.button0A.Click += new System.EventHandler(this.button0A_Click);
            this.button0A.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button0A_MouseDown);
            // 
            // button05
            // 
            this.button05.Location = new System.Drawing.Point(64, 164);
            this.button05.Name = "button05";
            this.button05.Size = new System.Drawing.Size(148, 23);
            this.button05.TabIndex = 27;
            this.button05.Text = "LIDAR distance";
            this.button05.UseVisualStyleBackColor = true;
            this.button05.Click += new System.EventHandler(this.button05_Click);
            this.button05.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button05_MouseDown);
            // 
            // checkBox0A
            // 
            this.checkBox0A.AutoSize = true;
            this.checkBox0A.Location = new System.Drawing.Point(43, 315);
            this.checkBox0A.Name = "checkBox0A";
            this.checkBox0A.Size = new System.Drawing.Size(15, 14);
            this.checkBox0A.TabIndex = 51;
            this.checkBox0A.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 170);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(30, 13);
            this.label10.TabIndex = 29;
            this.label10.Text = "0x05";
            // 
            // checkBox06
            // 
            this.checkBox06.AutoSize = true;
            this.checkBox06.Location = new System.Drawing.Point(43, 199);
            this.checkBox06.Name = "checkBox06";
            this.checkBox06.Size = new System.Drawing.Size(15, 14);
            this.checkBox06.TabIndex = 31;
            this.checkBox06.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(7, 286);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(30, 13);
            this.label18.TabIndex = 49;
            this.label18.Text = "0x09";
            // 
            // button06
            // 
            this.button06.Location = new System.Drawing.Point(64, 193);
            this.button06.Name = "button06";
            this.button06.Size = new System.Drawing.Size(148, 23);
            this.button06.TabIndex = 32;
            this.button06.Text = "Red Laser On";
            this.button06.UseVisualStyleBackColor = true;
            this.button06.Click += new System.EventHandler(this.button06_Click);
            this.button06.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button06_MouseDown);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(7, 199);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(30, 13);
            this.label14.TabIndex = 34;
            this.label14.Text = "0x06";
            // 
            // button09
            // 
            this.button09.Location = new System.Drawing.Point(64, 280);
            this.button09.Name = "button09";
            this.button09.Size = new System.Drawing.Size(148, 23);
            this.button09.TabIndex = 47;
            this.button09.Text = "IMU Data";
            this.button09.UseVisualStyleBackColor = true;
            this.button09.Click += new System.EventHandler(this.button09_Click);
            this.button09.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button09_MouseDown);
            // 
            // checkBox07
            // 
            this.checkBox07.AutoSize = true;
            this.checkBox07.Location = new System.Drawing.Point(43, 228);
            this.checkBox07.Name = "checkBox07";
            this.checkBox07.Size = new System.Drawing.Size(15, 14);
            this.checkBox07.TabIndex = 36;
            this.checkBox07.UseVisualStyleBackColor = true;
            // 
            // checkBox09
            // 
            this.checkBox09.AutoSize = true;
            this.checkBox09.Location = new System.Drawing.Point(43, 286);
            this.checkBox09.Name = "checkBox09";
            this.checkBox09.Size = new System.Drawing.Size(15, 14);
            this.checkBox09.TabIndex = 46;
            this.checkBox09.UseVisualStyleBackColor = true;
            // 
            // button07
            // 
            this.button07.Location = new System.Drawing.Point(64, 222);
            this.button07.Name = "button07";
            this.button07.Size = new System.Drawing.Size(148, 23);
            this.button07.TabIndex = 37;
            this.button07.Text = "RGB LEDs On";
            this.button07.UseVisualStyleBackColor = true;
            this.button07.Click += new System.EventHandler(this.button07_Click);
            this.button07.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button07_MouseDown);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(7, 228);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(30, 13);
            this.label16.TabIndex = 39;
            this.label16.Text = "0x07";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(7, 257);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(30, 13);
            this.label20.TabIndex = 44;
            this.label20.Text = "0x08";
            // 
            // checkBox08
            // 
            this.checkBox08.AutoSize = true;
            this.checkBox08.Location = new System.Drawing.Point(43, 257);
            this.checkBox08.Name = "checkBox08";
            this.checkBox08.Size = new System.Drawing.Size(15, 14);
            this.checkBox08.TabIndex = 41;
            this.checkBox08.UseVisualStyleBackColor = true;
            // 
            // button08
            // 
            this.button08.Location = new System.Drawing.Point(64, 251);
            this.button08.Name = "button08";
            this.button08.Size = new System.Drawing.Size(148, 23);
            this.button08.TabIndex = 42;
            this.button08.Text = "IR LED On";
            this.button08.UseVisualStyleBackColor = true;
            this.button08.Click += new System.EventHandler(this.button08_Click);
            this.button08.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button08_MouseDown);
            // 
            // button_start_test
            // 
            this.button_start_test.Enabled = false;
            this.button_start_test.Location = new System.Drawing.Point(5, 560);
            this.button_start_test.Name = "button_start_test";
            this.button_start_test.Size = new System.Drawing.Size(221, 45);
            this.button_start_test.TabIndex = 67;
            this.button_start_test.Text = "START TEST";
            this.button_start_test.UseVisualStyleBackColor = true;
            this.button_start_test.Click += new System.EventHandler(this.button_start_test_Click);
            this.button_start_test.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button_start_test_MouseDown);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(575, 22);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(48, 208);
            this.label27.TabIndex = 66;
            this.label27.Text = "0 - 0000\r\n1 - 0001\r\n2 - 0010\r\n3 - 0011\r\n4 - 0100\r\n5 - 0101\r\n6 - 0110\r\n7 - 0111\r\n8" +
    " - 1000\r\n9 - 1001\r\nA - 1010\r\nB - 1011\r\nC - 1100\r\nD - 1101\r\nE - 1110\r\nF - 1111";
            this.label27.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // textBox_terminal
            // 
            this.textBox_terminal.Location = new System.Drawing.Point(562, 12);
            this.textBox_terminal.Multiline = true;
            this.textBox_terminal.Name = "textBox_terminal";
            this.textBox_terminal.Size = new System.Drawing.Size(349, 682);
            this.textBox_terminal.TabIndex = 3;
            // 
            // serialPort1
            // 
            this.serialPort1.BaudRate = 115200;
            this.serialPort1.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPort1_DataReceived);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Emitter_Main_Board_Tester.Properties.Resources.Micro_C_Logo;
            this.pictureBox1.Location = new System.Drawing.Point(396, 20);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(158, 42);
            this.pictureBox1.TabIndex = 67;
            this.pictureBox1.TabStop = false;
            // 
            // timer1_btn_rx
            // 
            this.timer1_btn_rx.Enabled = true;
            this.timer1_btn_rx.Interval = 50;
            this.timer1_btn_rx.Tick += new System.EventHandler(this.timer1_btn_rx_Tick);
            // 
            // timer_RX
            // 
            this.timer_RX.Enabled = true;
            this.timer_RX.Interval = 1;
            this.timer_RX.Tick += new System.EventHandler(this.timer_RX_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(923, 700);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.textBox_terminal);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label27);
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Emitter Main Board Tester";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button_Disconect;
        private System.Windows.Forms.Button button_Connect;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button button_Rescan;
        private System.Windows.Forms.CheckBox checkBox00;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textBox0C;
        private System.Windows.Forms.Button button0C;
        private System.Windows.Forms.CheckBox checkBox0C;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox textBox0B;
        private System.Windows.Forms.Button button0B;
        private System.Windows.Forms.CheckBox checkBox0B;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox textBox0A;
        private System.Windows.Forms.Button button0A;
        private System.Windows.Forms.CheckBox checkBox0A;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBox09_0;
        private System.Windows.Forms.Button button09;
        private System.Windows.Forms.CheckBox checkBox09;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBox07_0;
        private System.Windows.Forms.Button button08;
        private System.Windows.Forms.CheckBox checkBox08;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBox08;
        private System.Windows.Forms.Button button07;
        private System.Windows.Forms.CheckBox checkBox07;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBox06;
        private System.Windows.Forms.Button button06;
        private System.Windows.Forms.CheckBox checkBox06;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBox05;
        private System.Windows.Forms.Button button05;
        private System.Windows.Forms.CheckBox checkBox05;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBox04;
        private System.Windows.Forms.Button button04;
        private System.Windows.Forms.CheckBox checkBox04;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox03;
        private System.Windows.Forms.Button button03;
        private System.Windows.Forms.CheckBox checkBox03;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox02;
        private System.Windows.Forms.Button button02;
        private System.Windows.Forms.CheckBox checkBox02;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox01;
        private System.Windows.Forms.Button button01;
        private System.Windows.Forms.CheckBox checkBox01;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox00;
        private System.Windows.Forms.Button button00;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Button button_start_test;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox textBox_terminal;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox textBox09_1;
        private System.Windows.Forms.TextBox textBox07_1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox textBox_IMU0;
        private System.Windows.Forms.TextBox textBox_IMU8;
        private System.Windows.Forms.TextBox textBox_IMU1;
        private System.Windows.Forms.TextBox textBox_IMU7;
        private System.Windows.Forms.TextBox textBox_IMU2;
        private System.Windows.Forms.TextBox textBox_IMU6;
        private System.Windows.Forms.TextBox textBox_IMU3;
        private System.Windows.Forms.TextBox textBox_IMU5;
        private System.Windows.Forms.TextBox textBox_IMU4;
        private System.Windows.Forms.TextBox textBox09_R;
        private System.Windows.Forms.TextBox textBox03_1_R;
        private System.Windows.Forms.TextBox textBox03_0_R;
        private System.Windows.Forms.TextBox textBox04_2_R;
        private System.Windows.Forms.TextBox textBox04_1_R;
        private System.Windows.Forms.TextBox textBox04_0_R;
        private System.Windows.Forms.TextBox textBox05_2_R;
        private System.Windows.Forms.TextBox textBox05_1_R;
        private System.Windows.Forms.TextBox textBox07_1_R;
        private System.Windows.Forms.TextBox textBox00_R;
        private System.Windows.Forms.TextBox textBox01_R;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox textBox02_R;
        private System.Windows.Forms.TextBox textBox0C_R;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox textBox0B_R;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox textBox0A_R;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox textBox05_0_R;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox textBox06_R;
        private System.Windows.Forms.TextBox textBox07_0_R;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox textBox08_R;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Button button_READALL;
        private System.Windows.Forms.Button button_WRITEALL;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Button buttonLED_RX;
        private System.Windows.Forms.Button buttonLED_TX;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox textBox_calc_send;
        public System.Windows.Forms.Timer timer1_btn_rx;
        public System.Windows.Forms.Timer timer_RX;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox textBoxMD;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox textBoxREV;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox textBoxSN;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Button buttonMD;
        private System.Windows.Forms.CheckBox checkBoxMD;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Button buttonREV;
        private System.Windows.Forms.CheckBox checkBoxREV;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Button buttonSN;
        private System.Windows.Forms.CheckBox checkBoxSN;
    }
}

