﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Threading;



namespace Emitter_Main_Board_Tester
{
    public partial class Form1 : Form
    {

volatile byte sequence = 0;
volatile byte LED_TX = 0;
volatile byte LED_RX = 0;
        volatile byte Timer_RX = 0;


        public Form1()
        {
            InitializeComponent();
        }

        private void button_Rescan_Click(object sender, EventArgs e)
        {
            try

            {
                string[] ports = SerialPort.GetPortNames();//serialPort1.GetPortNames();// Get names ports

                //
                comboBox1.Items.Clear();// clear list

                comboBox1.Items.AddRange(ports);// Add ports to list
                comboBox1.SelectedIndex = 0;
                if (ports.Length != 0) button_Connect.Enabled = true;
                else button_Connect.Enabled = false;
                comboBox1.Enabled = true;
            }
            catch
            {
                comboBox1.Enabled = false;
                // button4_send.Enabled = false;
                button_Connect.Enabled = false;

                button_start_test.Enabled = false;
                comboBox1.Items.Clear();// Clear list
                comboBox1.ResetText();// clear box



            }
        }

        private void button_Connect_Click(object sender, EventArgs e)
        {
            //checkBox_whilesend.Enabled = true;
            button_start_test.Enabled = true;
            button_Disconect.Enabled = true;
            button_Connect.Enabled = false;
            //button4_send.Enabled = true;
            button_Rescan.Enabled = false;
           // button4_send.Enabled = true;
            //timer2.Enabled = true;
            try
            {
                serialPort1.PortName = comboBox1.Text;

                //port.PortName = ports[num];
                serialPort1.BaudRate = 115200;
                serialPort1.DataBits = 8;
                serialPort1.Parity = System.IO.Ports.Parity.None;
                serialPort1.StopBits = System.IO.Ports.StopBits.One;
                serialPort1.ReadTimeout = 1000;
                serialPort1.WriteTimeout = 1000;


                serialPort1.Open();


            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            DateTime date = new DateTime();
            textBox_terminal.Text = Convert.ToString(DateTime.Now);

        }

        private void button_Disconect_Click(object sender, EventArgs e)
        {
            button_start_test.Enabled = false;
            //checkBox_whilesend.Checked = false;
            //checkBox_whilesend.Enabled = false;
            button_Disconect.Enabled = false;
            button_Connect.Enabled = false;
            button_Disconect.Enabled = false;
            button_Rescan.Enabled = true;
            //button4_send.Enabled = false;
            //timer2.Enabled = false;
            try
            {
                //serialPort1.PortName = cmbCom.Text;
                serialPort1.Close();


            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void button_start_test_Click(object sender, EventArgs e)
        {
            button_WRITEALL_Click(0, e);


            try
                {

                    //serialPort1.Write(writestring, 0, writestring.Length);   //OK!
                    //if (serialPort1.IsOpen) { serialPort1.Write(data, 0, data.Length); };
                    if (serialPort1.IsOpen)
                {
                   // serialPort1.Write("OK!");
                  //  send_data(1);//cmd data0 data1 data_count

                };//Convert.ToString(rawdata)


                }
                catch (Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            
            
        }

        private void button_start_test_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                try
                {

                    //serialPort1.Write(writestring, 0, writestring.Length);   //OK!
                    //if (serialPort1.IsOpen) { serialPort1.Write(data, 0, data.Length); };
                   // if (serialPort1.IsOpen) { serialPort1.Write("07.09.2018 Alex_403"); };//Convert.ToString(rawdata)

                }
                catch (Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            }
        }

        private void timer1_btn_rx_Tick(object sender, EventArgs e)
        {
            if (LED_TX != 0) { buttonLED_TX.BackColor = Color.Lime; LED_TX--; }
            else {buttonLED_TX.BackColor = Color.FromArgb(0, 128, 128, 128); }

            if (LED_RX != 0) { buttonLED_RX.BackColor = Color.Lime; LED_RX--; }
            else { buttonLED_RX.BackColor = Color.FromArgb(0, 128, 128, 128); }

            //buttonLED_RX.BackColor = Color.FromArgb(0, 128, 128, 128);
           // timer1_btn_rx.Enabled = false;


        }

        private void serialPort1_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            LED_RX = 5;
            //buttonLED_RX.BackColor = Color.Lime; Thread.Sleep(15);
            //timer1_btn_rx.Enabled = true;
            //Timer_RX = 2;
            //Thread.Sleep(1);
            //string message = Convert.ToString(serialPort1.ReadExisting());
            //textBox09_R.Text = message;
            Timer_RX = 1;
/*
            try
            {
                textBox_terminal.AppendText("<<" + message + Convert.ToString((char)10));
            }
            catch { };
            */
        }

        void send_data(byte cmd)
        {
            
           
            //Thread.Sleep(15);
            LED_TX = 5;
            //string message = Convert.ToString(serialPort1.ReadExisting());

            //byte[] rx_data= { 0};
            //string message = Convert.ToString(Byte.Parse(Convert.ToString(serialPort1.ReadExisting()), System.Globalization.NumberStyles.HexNumber));
            //string message = serialPort1.ReadExisting();

            //serialPort1.Write(message);


           // textBox09_R.Text = message;
            

            string rawdata = textBox_calc_send.Text;
            

            try
            {
                if (serialPort1.IsOpen)
                {
                textBox_terminal.AppendText(Convert.ToString((char)13)+Convert.ToString((char)10) + ">> " + rawdata);
                //textBox_terminal.AppendText(rawdata);
                //textBox_terminal.AppendText(Convert.ToString((char)10));

                rawdata = rawdata.Replace(" ", string.Empty); //clear " "
                    byte[] data = Enumerable.Range(0, rawdata.Length / 2).Select(x => Convert.ToByte(rawdata.Substring(x * 2, 2), 16)).ToArray();

                    serialPort1.Write(data, 0, data.Length);
                };


            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Message", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            //buttonLED_TX.BackColor = Color.FromArgb(0, 128, 128, 128);
        }


        private void Calc_Data (byte button, byte cmdin)
        {
            byte[] seq = { 0 };
            byte[] cmd = { 0 };
            byte[] reg = { 0 };
            byte[] data = { 0 };
            byte[] data2 = { 0 };
            byte[] len = { 0 };
            byte[] cr = { 0 };
            byte crc = 0;
            len[0]++; len[0]++;

            /////////   0xC0  LEN  SEQ   CMD   DATA  DATAn   CS   0xCO

            ///////////////////////////////////////////////////// SEQ
                seq[0] = sequence++; len[0]++;
       

            // string data2 = "010203FF";
            //byte[] i = Convert.ToSByte(data2);
            //textBox_OutData.AppendText(" ");
            // textBox_OutData.AppendText(Convert.ToString( i));
            //textBox_OutData.AppendText(" ");


            //////////////////////////////////////////////////////CMD
            cmd[0] = cmdin; len[0]++;
            //textBox_OutData.AppendText(BitConverter.ToString(cmd));


            //////////////////////////////////////////////////////REG
            switch (button)
            {
                case 0:
                    reg[0] = 0x00; len[0]++;
                break;

                case 1:
                    reg[0] = 0x01; len[0]++;
                    break;

                case 2:
                    reg[0] = 0x02; len[0]++;
                break;

                case 3:
                    reg[0] = 0x03; len[0]++;
                break;

                case 4:
                    reg[0] = 0x04; len[0]++;
                 break;

                case 5:
                    reg[0] = 0x05; len[0]++;
                break;

                case 6:
                    reg[0] = 0x06; len[0]++;
                break;

                case 7:
                    reg[0] = 0x07; len[0]++;
                break;

                case 8:
                    reg[0] = 0x08; len[0]++;
                break;

                case 9:
                    reg[0] = 0x09; len[0]++;
                break;

                case 0x0A:
                    reg[0] = 0x0A; len[0]++;
                break;

                case 0x0B:
                    reg[0] = 0x0B; len[0]++;
                break;

                case 0x0C:
                    reg[0] = 0X0C; len[0]++;
                break;

                default:
                    reg[0] = 0xFF; len[0]++;
                break;
            }


            //////////////////////////////////////////////////////DATA
            switch (button)
            {
                case 0:
                    data[0] = 0; len[0]++; cmd[0] = 0;
                break;

                case 1:
                    data[0] = Byte.Parse(textBox01.Text, System.Globalization.NumberStyles.HexNumber); len[0]++;
                break;

                case 2:
                    data[0] = 0; len[0]++; cmd[0] = 0;
                break;

                case 3:
                    data[0] = 0; len[0]++; cmd[0] = 0;
                break;

                case 4:
                    data[0] = 0; len[0]++; cmd[0] = 0;
                break;

                case 5:
                    data[0] = 0; len[0]++; cmd[0] = 0;
                break;

                case 6:
                    data[0] = Byte.Parse(textBox06.Text, System.Globalization.NumberStyles.HexNumber); len[0]++;
                break;

                case 7:
                    data[0] =  Byte.Parse(textBox07_0.Text, System.Globalization.NumberStyles.HexNumber); len[0]++;
                    data2[0] = Byte.Parse(textBox07_1.Text, System.Globalization.NumberStyles.HexNumber); len[0]++;
                break;

                case 8:
                    data[0] = Byte.Parse(textBox08.Text, System.Globalization.NumberStyles.HexNumber); len[0]++;
                break;

                case 9:
                    data[0] =  Byte.Parse(textBox09_0.Text, System.Globalization.NumberStyles.HexNumber); len[0]++;
                    if (data[0] != 0x01) { data2[0] = Byte.Parse(textBox09_1.Text, System.Globalization.NumberStyles.HexNumber); len[0]++;} 
                break;

                case 10:
                    data[0] = Byte.Parse(textBox0A.Text, System.Globalization.NumberStyles.HexNumber); len[0]++;
                break;

                case 11:
                    data[0] = Byte.Parse(textBox0B.Text, System.Globalization.NumberStyles.HexNumber); len[0]++;
                break;

                case 12:
                    data[0] = Byte.Parse(textBox0C.Text, System.Globalization.NumberStyles.HexNumber); len[0]++;
                break;

                default:
                    data[0] = 0x00; len[0]++; cmd[0] = 0;
                break;
            }

            if (cmdin == 0) { data[0] = 0; len[0] = 6; }
            //textBox_OutData.AppendText(BitConverter.ToString(reg));


            textBox_calc_send.Clear();// clear output box
            textBox_calc_send.AppendText("C0 ");//send start
            textBox_calc_send.AppendText(BitConverter.ToString(len)+" ");// send lenght frame, not a start/end

            if (seq[0] == 0xC0) { textBox_calc_send.AppendText("DB DC "); }
            else if (seq[0] == 0xDB) { textBox_calc_send.AppendText("DB DD "); }
            else { textBox_calc_send.AppendText(BitConverter.ToString(seq)+" "); }//seq 

            textBox_calc_send.AppendText(BitConverter.ToString(cmd)+" ");//send command

            textBox_calc_send.AppendText(BitConverter.ToString(reg) + " ");//send reg


            if (data[0] == 0xC0) { textBox_calc_send.AppendText("DB DC "); }
            else if (data[0] == 0xDB) { textBox_calc_send.AppendText("DB DD "); }
            else
            { textBox_calc_send.AppendText(BitConverter.ToString(data) + " "); }//send data

            if (len[0] == 7)
            {
                if (data2[0] == 0xC0) { textBox_calc_send.AppendText("DB DC "); }
                else if (data2[0] == 0xDB) { textBox_calc_send.AppendText("DB DD "); }
                else
                { textBox_calc_send.AppendText(BitConverter.ToString(data2) + " "); }//send data byte 2
            }

            crc += len[0]; crc += seq[0]; crc += cmd[0]; crc += reg[0]; crc += data[0];// calc crc
            if (len[0] == 7) { crc += data2[0]; };

            cr[0] = ((byte)~crc); cr[0]++;//calc crc
            
            if (cr[0] == 0xC0) { textBox_calc_send.AppendText("DB DC "); }
            else if (cr[0] == 0xDB) { textBox_calc_send.AppendText("DB DD "); }
            else
            { textBox_calc_send.AppendText(BitConverter.ToString(cr) + " "); }//send CRC

            textBox_calc_send.AppendText("C0");//send END


        }

        private void button00_Click(object sender, EventArgs e)
        {
           
            Calc_Data(0x00, 0x01);
            if (serialPort1.IsOpen) { send_data(0); }

         }


        private void button01_Click(object sender, EventArgs e)
        {
            if (textBox01.Text == "") { textBox01.Text = "00"; }
            Calc_Data(0x01, 0x01);
            if (serialPort1.IsOpen) { send_data(0); }
        }

        private void button02_Click(object sender, EventArgs e)
        {
            Calc_Data(0x02, 0x01);
            if (serialPort1.IsOpen) { send_data(0); }
        }

        private void button03_Click(object sender, EventArgs e)
        {
            Calc_Data(0x03, 0x01);
            if (serialPort1.IsOpen) { send_data(0); }
        }

        private void button04_Click(object sender, EventArgs e)
        {
            Calc_Data(0x04, 0x01);
            if (serialPort1.IsOpen) { send_data(0); }
        }

        private void button05_Click(object sender, EventArgs e)
        {
            Calc_Data(0x05, 0x01);
            if (serialPort1.IsOpen) { send_data(0); }
        }

        private void button06_Click(object sender, EventArgs e)
        {
            if (textBox06.Text == "") { textBox06.Text = "00"; }
            Calc_Data(0x06, 0x01);
            if (serialPort1.IsOpen) { send_data(0); }
        }

        private void button07_Click(object sender, EventArgs e)
        {
            if (textBox07_0.Text == "") { textBox07_0.Text = "00"; }
            if (textBox07_1.Text == "") { textBox07_1.Text = "00"; }
            Calc_Data(0x07, 0x01);
            if (serialPort1.IsOpen) { send_data(0); }
        }

        private void button08_Click(object sender, EventArgs e)
        {
            if (textBox08.Text == "") { textBox08.Text = "00"; }
            Calc_Data(0x08, 0x01);
            if (serialPort1.IsOpen) { send_data(0); }
        }

        private void button09_Click(object sender, EventArgs e)
        {
            if (textBox09_0.Text == "") { textBox09_0.Text = "00"; }
            if (textBox09_1.Text == "") { textBox09_1.Text = "00"; }
            Calc_Data(0x09, 0x01);
            if (serialPort1.IsOpen) { send_data(0); }
        }

        private void button0A_Click(object sender, EventArgs e)
        {
            if (textBox0A.Text == "") { textBox0A.Text = "00"; }
            Calc_Data(0x0A, 0x01);
            if (serialPort1.IsOpen) { send_data(0); }
        }

        private void button0B_Click(object sender, EventArgs e)
        {
            if (textBox0B.Text == "") { textBox0B.Text = "00"; }
            Calc_Data(0x0B, 0x01);
            if (serialPort1.IsOpen) { send_data(0); }
        }

        private void button0C_Click(object sender, EventArgs e)
        {
            if (textBox0C.Text == "") { textBox0C.Text = "00"; }
            Calc_Data(012, 0x01);
            if (serialPort1.IsOpen) { send_data(0); }
        }

        private void button_WRITEALL_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen) { LED_TX = 15; }
            
            button00_Click(0,e);
            button01_Click(0, e);
            button02_Click(0, e);
            button03_Click(0, e);
            button04_Click(0, e);
            button05_Click(0, e);
            button06_Click(0, e);
            button07_Click(0, e);
            button08_Click(0, e);
            button09_Click(0, e);
            button0A_Click(0, e);
            button0B_Click(0, e);
            button0C_Click(0, e);

            /*
button00.PerformClick();
button01.PerformClick(); 
button02.PerformClick();
button03.PerformClick();
button04.PerformClick(); 
button05.PerformClick();
button06.PerformClick(); 
button07.PerformClick(); 
button08.PerformClick(); 
button09.PerformClick(); 
button0A.PerformClick(); 
button0B.PerformClick(); 
button0C.PerformClick();
buttonLED_TX.BackColor = Color.FromArgb(0, 128, 128, 128);
*/


        }

        private void button_READALL_Click(object sender, EventArgs e)
        {
            buttonLED_TX.BackColor = Color.Lime; timer1_btn_rx.Enabled = true;
            MouseEventArgs k = new MouseEventArgs(MouseButtons.Right, 1, 0, 0, 0);
            button00_MouseDown(this, k); buttonLED_TX.BackColor = Color.Lime; timer1_btn_rx.Enabled = true;
            button01_MouseDown(this, k); buttonLED_TX.BackColor = Color.Lime; timer1_btn_rx.Enabled = true;
            button02_MouseDown(this, k); buttonLED_TX.BackColor = Color.Lime; timer1_btn_rx.Enabled = true;
            button03_MouseDown(this, k); buttonLED_TX.BackColor = Color.Lime; timer1_btn_rx.Enabled = true;
            button04_MouseDown(this, k); buttonLED_TX.BackColor = Color.Lime; timer1_btn_rx.Enabled = true;
            button05_MouseDown(this, k); buttonLED_TX.BackColor = Color.Lime; timer1_btn_rx.Enabled = true;
            button06_MouseDown(this, k); buttonLED_TX.BackColor = Color.Lime; timer1_btn_rx.Enabled = true;
            button07_MouseDown(this, k); buttonLED_TX.BackColor = Color.Lime; timer1_btn_rx.Enabled = true;
            button08_MouseDown(this, k); buttonLED_TX.BackColor = Color.Lime; timer1_btn_rx.Enabled = true;
            button09_MouseDown(this, k); buttonLED_TX.BackColor = Color.Lime; timer1_btn_rx.Enabled = true;
            button0A_MouseDown(this, k); buttonLED_TX.BackColor = Color.Lime; timer1_btn_rx.Enabled = true;
            button0B_MouseDown(this, k); buttonLED_TX.BackColor = Color.Lime; timer1_btn_rx.Enabled = true;
            button0C_MouseDown(this, k); buttonLED_TX.BackColor = Color.Lime; timer1_btn_rx.Enabled = true;




        }

        private void button00_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                try
                {

                    //serialPort1.Write(writestring, 0, writestring.Length);   //OK!
                    //if (serialPort1.IsOpen) { serialPort1.Write(data, 0, data.Length); };
                    //if (serialPort1.IsOpen) { serialPort1.Write("07.09.2018 Alex_403"); };//Convert.ToString(rawdata)
                    Calc_Data(0x00, 0x00);
                    if (serialPort1.IsOpen) { send_data(0); }
                }
                catch (Exception ex) { MessageBox.Show(ex.Message, "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            }
        }

        private void button01_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (textBox01.Text == "") { textBox01.Text = "00"; }
                Calc_Data(0x01, 0x00);
                    if (serialPort1.IsOpen) { send_data(0); }

            }
        }

        private void button02_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                try
                {

                    //serialPort1.Write(writestring, 0, writestring.Length);   //OK!
                    //if (serialPort1.IsOpen) { serialPort1.Write(data, 0, data.Length); };
                    //if (serialPort1.IsOpen) { serialPort1.Write("07.09.2018 Alex_403"); };//Convert.ToString(rawdata)
                    Calc_Data(0x02, 0x00);
                    if (serialPort1.IsOpen) { send_data(0); }
                }
                catch (Exception ex) { MessageBox.Show(ex.Message, "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            }
        }

        private void button03_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                try
                {

                    //serialPort1.Write(writestring, 0, writestring.Length);   //OK!
                    //if (serialPort1.IsOpen) { serialPort1.Write(data, 0, data.Length); };
                    //if (serialPort1.IsOpen) { serialPort1.Write("07.09.2018 Alex_403"); };//Convert.ToString(rawdata)
                    Calc_Data(0x03, 0x00);
                    if (serialPort1.IsOpen) { send_data(0); }
                }
                catch (Exception ex) { MessageBox.Show(ex.Message, "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            }
        }

        private void button04_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                try
                {

                    //serialPort1.Write(writestring, 0, writestring.Length);   //OK!
                    //if (serialPort1.IsOpen) { serialPort1.Write(data, 0, data.Length); };
                    //if (serialPort1.IsOpen) { serialPort1.Write("07.09.2018 Alex_403"); };//Convert.ToString(rawdata)
                    Calc_Data(0x04, 0x00);
                    if (serialPort1.IsOpen) { send_data(0); }
                }
                catch (Exception ex) { MessageBox.Show(ex.Message, "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            }
        }


        private void button05_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                try
                {

                    //serialPort1.Write(writestring, 0, writestring.Length);   //OK!
                    //if (serialPort1.IsOpen) { serialPort1.Write(data, 0, data.Length); };
                    //if (serialPort1.IsOpen) { serialPort1.Write("07.09.2018 Alex_403"); };//Convert.ToString(rawdata)
                    Calc_Data(0x05, 0x00);
                    if (serialPort1.IsOpen) { send_data(0); }
                }
                catch (Exception ex) { MessageBox.Show(ex.Message, "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            }
        }

        private void button06_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (textBox06.Text == "") { textBox06.Text = "00"; }
                Calc_Data(0x06, 0x00);
                if (serialPort1.IsOpen) { send_data(0); }

            }
        }

        private void button07_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (textBox07_0.Text == "") { textBox07_0.Text = "00"; }
                if (textBox07_1.Text == "") { textBox07_1.Text = "00"; }
                Calc_Data(0x07, 0x00);
                if (serialPort1.IsOpen) { send_data(0); }

            }
        }

        private void button08_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (textBox08.Text == "") { textBox08.Text = "00"; }
                Calc_Data(0x08, 0x00);
                if (serialPort1.IsOpen) { send_data(0); }

            }
        }

        private void button09_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (textBox09_0.Text == "") { textBox09_0.Text = "00"; }
                if (textBox09_1.Text == "") { textBox09_1.Text = "00"; }
                Calc_Data(0x09, 0x00);
                if (serialPort1.IsOpen) { send_data(0); }

            }
        }

        private void button0A_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (textBox0A.Text == "") { textBox0A.Text = "00"; }
                Calc_Data(0x0A, 0x00);
                if (serialPort1.IsOpen) { send_data(0); }

            }
        }

        private void button0B_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (textBox0B.Text == "") { textBox0B.Text = "00"; }
                Calc_Data(0x0B, 0x00);
                if (serialPort1.IsOpen) { send_data(0); }

            }
        }

        private void button0C_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (textBox0C.Text == "") { textBox0C.Text = "00"; }
                Calc_Data(0x0C, 0x00);
                if (serialPort1.IsOpen) { send_data(0); }

            }
        }

        private void timer_RX_Tick(object sender, EventArgs e)
        {
            Thread.Sleep(1);
            timer_RX.Enabled = false;
            

            if (serialPort1.IsOpen & Timer_RX==1)
            {
                //string message = Convert.ToString(serialPort1.ReadExisting());
                byte[] data = new byte[serialPort1.BytesToRead+25];
                serialPort1.Read(data, 0, data.Length-25);
                var hexString = BitConverter.ToString(data,0, data.Length - 25);
                hexString = hexString.Replace("-", " ");
                textBox_terminal.AppendText(Convert.ToString((char)13) + Convert.ToString((char)10) + "<< " + hexString);

                for (int i = 0; i < (data.Length-25); i++)
                {
                    if (data[i] == 0xC0)
                    {
                        byte crc=0;
                        byte len = data[++i];
                        crc += len;

                        byte seq = data[++i];
                        if ((seq == 0xDB) & (data[i + 1] == 0xDC)) { seq = 0xC0; i++; }
                        else if ((seq == 0xDB) & (data[i + 1] == 0xDD)) { seq = 0xDB; i++; }
                        crc += seq;

                        byte cmd = data[++i];
                        crc += cmd;

                        byte[] data2 = new byte[20];// { 0,0,0,0,0,0,0,0,0};
                    
                        for(byte ii=0; ii< (len-4);ii++)
                        {
                            data2[ii]= data[++i];
                            if ((data2[ii] == 0xDB) & (data[i + 1] == 0xDC)) { data2[ii] = 0xC0; i++; }
                            else if ((data2[ii] == 0xDB) & (data[i + 1] == 0xDD)) { data2[ii] = 0xDB; i++; }
                            
                        }

                        for (byte ii = 0; ii < (len - 4); ii++)
                        {
                            crc += data2[ii];
                        }


                        byte cs = data[++i];
                        if ((cs == 0xDB) & (data[i + 1] == 0xDC)) { cs = 0xC0; i++; }
                        else if ((cs == 0xDB) & (data[i + 1] == 0xDD)) { cs = 0xDB; i++; }

                        if (data[++i] == 0xC0)
                        {
                            
                            crc = ((byte)~crc);crc++;

                            textBox_terminal.AppendText(" Frame OK!");

                            if (crc == cs) textBox_terminal.AppendText(" CRC OK!"); else textBox_terminal.AppendText(" CRC ERR!");

                            if (crc == cs)
                            {
                                switch (data2[0])
                                {
                                    case 0:
                                        textBox00_R.Text = "";
                                        textBox00_R.AppendText(BitConverter.ToString(data2,1,1));
                                        checkBox00.Checked = true;
                                        label1.BackColor = Color.Lime;
                                    break;
                                    case 1:
                                        textBox01_R.Text = "";
                                        textBox01_R.AppendText(BitConverter.ToString(data2, 1, 1));
                                        checkBox01.Checked = true;
                                        label2.BackColor = Color.Lime;
                                        break;
                                    case 2:
                                        textBox02_R.Text = "";
                                        textBox02_R.AppendText(BitConverter.ToString(data2, 1, 1));
                                        checkBox02.Checked = true;
                                        label6.BackColor = Color.Lime;
                                        break;
                                    case 3:
                                        textBox03_0_R.Text = "";
                                        textBox03_0_R.AppendText(BitConverter.ToString(data2, 1, 1));
                                        textBox03_1_R.Text = "";
                                        textBox03_1_R.AppendText(BitConverter.ToString(data2, 2, 1));
                                        checkBox03.Checked = true;
                                        label8.BackColor = Color.Lime;
                                        break;
                                    case 4:
                                        textBox04_0_R.Text = "";
                                        textBox04_0_R.AppendText(BitConverter.ToString(data2, 1, 1));
                                        textBox04_1_R.Text = "";
                                        textBox04_1_R.AppendText(BitConverter.ToString(data2, 2, 1));
                                        textBox04_2_R.Text = "";
                                        textBox04_2_R.AppendText(BitConverter.ToString(data2, 3, 1));
                                        checkBox04.Checked = true;
                                        label12.BackColor = Color.Lime;
                                        break;
                                    case 5:
                                        textBox05_0_R.Text = "";
                                        textBox05_0_R.AppendText(BitConverter.ToString(data2, 1, 1));
                                        textBox05_1_R.Text = "";
                                        textBox05_1_R.AppendText(BitConverter.ToString(data2, 2, 1));
                                        textBox05_2_R.Text = "";
                                        textBox05_2_R.AppendText(BitConverter.ToString(data2, 3, 1));
                                        checkBox05.Checked = true;
                                        label10.BackColor = Color.Lime;
                                        break;
                                    case 6:
                                        textBox06_R.Text = "";
                                        textBox06_R.AppendText(BitConverter.ToString(data2, 1, 1));
                                        checkBox06.Checked = true;
                                        label14.BackColor = Color.Lime;
                                        break;
                                    case 7:
                                        textBox07_0_R.Text = "";
                                        textBox07_0_R.AppendText(BitConverter.ToString(data2, 1, 1));
                                        textBox07_1_R.Text = "";
                                        textBox07_1_R.AppendText(BitConverter.ToString(data2, 2, 1));
                                        checkBox07.Checked = true;
                                        label16.BackColor = Color.Lime;
                                        break;
                                    case 8:
                                        textBox08_R.Text = "";
                                        textBox08_R.AppendText(BitConverter.ToString(data2, 1, 1));
                                        checkBox08.Checked = true;
                                        label20.BackColor = Color.Lime;
                                        break;
                                    case 9:
                                        textBox09_R.Text = "";
                                        var hex = BitConverter.ToString(data2, 1, len - 5);
                                        hex = hex.Replace("-", " ");
                                        textBox09_R.AppendText(hex);
                                        checkBox09.Checked = true;
                                        hex = hex.Replace(" ", "");
                                        label18.BackColor = Color.Lime;
                                        break;
                                    case 10:
                                        textBox0A_R.Text = "";
                                        textBox0A_R.AppendText(BitConverter.ToString(data2, 1, 1));
                                        checkBox0A.Checked = true;
                                        label26.BackColor = Color.Lime;
                                        break;
                                    case 11:
                                        textBox0B_R.Text = "";
                                        textBox0B_R.AppendText(BitConverter.ToString(data2, 1, 1));
                                        checkBox0B.Checked = true;
                                        label24.BackColor = Color.Lime;
                                        break;
                                    case 12:
                                        textBox0C_R.Text = "";
                                        textBox0C_R.AppendText(BitConverter.ToString(data2, 1, 1));
                                        checkBox0C.Checked = true;
                                        label22.BackColor = Color.Lime;
                                        break;

                                    case 0xF0:
                                      //  textBox0C_R.Text = "";
                                      //  textBox0C_R.AppendText(BitConverter.ToString(data2, 1, 1));
                                     //   checkBox0C.Checked = true;
                                        label46.BackColor = Color.Lime;
                                        break;

                                    default:


                                        break;





                                }





                            }








                        }


                    }

                }


//                byte[] ba; serialPort1.Read( ba, 0,serialPort1.ReadBufferSize);//serialPort1.ReadExisting();

                //byte[] ba = Encoding.Default.GetBytes(message);
                
                //byte[] data = Enumerable.Range(0, message.Length / 2).Select(x => Convert.ToByte(message.Substring(x * 2, 2), 16)).ToArray();
                // textBox09_R.Text = message;
                Timer_RX = 0;


                //textBox_terminal.AppendText(Convert.ToString((char)13) + Convert.ToString((char)10) + "<< " + hexString); //OK!
            }

           // if (Timer_RX == 0) { Timer_RX = 1; };

            timer_RX.Enabled = true;
        }


        void Write_File(string message) {




        }

        private void textBox01_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox01_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 'A') && (e.KeyChar <= 'F') || (e.KeyChar >= '0') && (e.KeyChar <= '9'))
            {
                return;
                
                                      
            }
          

            if ((e.KeyChar >= 'a') && (e.KeyChar <= 'f')) { e.KeyChar = (char)(e.KeyChar-32); return; } //change a-f >> A-F

            if (Char.IsControl(e.KeyChar)) { if (e.KeyChar == (char)Keys.Back) { Text.Remove(1); }; return; }

          


            e.Handled = true;
        }
    }
}
